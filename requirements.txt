fastapi==0.111.0
pydantic==2.7.2
pydantic-settings==2.2.1
pydantic_core==2.18.3
pymongo==4.7.2
python-dotenv==1.0.1
uvicorn==0.30.0
