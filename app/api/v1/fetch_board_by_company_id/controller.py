from http import HTTPMethod
from typing import List, Mapping, Sequence

from pymongo.collection import Collection
from pymongo.cursor import Cursor
from pymongo.errors import OperationFailure

from app.api.v1.fetch_board_by_company_id.exceptions import FetchBoardByCompanyIdException, FetchBoardException
from app.api.v1.fetch_board_by_company_id.models import (
    FetchBoardByCompanyIdRequest,
    FetchBoardByCompanyIdResponse,
    BoardResponse, TaskResponse
)
from app.core.api.base_controller import BaseAPIController
from app.core.api.collections import DBCollectionEnum
from app.services.tm_db.provider import TMMongoDBServiceProvider


class APIController(
    BaseAPIController[FetchBoardByCompanyIdRequest, FetchBoardByCompanyIdResponse],
    TMMongoDBServiceProvider
):
    """
    **Synchronous API endpoint for fetching all boards by company_id**

    Description:
        Fetches all boards by company_id

    """
    _full_dir: str = __file__
    api_tags: List[str] = ['Synchronous API']

    def get_path(self) -> str:
        return '/fetch_board_by_company_id'

    def get_method(self) -> str:
        """
        return HTTP method intended
        """
        return HTTPMethod.POST

    def _get_tasks(self, board_id: str) -> Sequence[TaskResponse]:
        nosql_service: Collection = self.get_mongodb_service_from_collection(DBCollectionEnum.TASKS.value)
        tasks: Cursor[Mapping] = nosql_service.find({'board_id': board_id}).sort('position', 1)

        return [
            TaskResponse(
                id=str(task.get('_id')),
                position=task.get('position'),
                name=task.get('name'),
                description=task.get('description'),
                board_id=task.get('board_id'),
                date_created=task.get('date_created'),
                date_updated=task.get('date_updated')
            )
            for task in tasks
        ]

    def process_request(self, request: FetchBoardByCompanyIdRequest) -> FetchBoardByCompanyIdResponse:
        try:
            nosql_service: Collection = self.get_mongodb_service_from_collection(request.collection)
            boards = nosql_service.find({'company_id': request.company_id}).sort('position', 1)

            return FetchBoardByCompanyIdResponse(
                boards=[
                    BoardResponse(
                        id=str(board.get('_id')),
                        position=board.get('position'),
                        name=board.get('name'),
                        description=board.get('description'),
                        company_id=board.get('company_id'),
                        date_created=board.get('date_created'),
                        date_updated=board.get('date_updated'),
                        tasks=self._get_tasks(str(board.get('_id')))
                    )
                    for board in boards
                ]
            )
        except OperationFailure as e:
            raise FetchBoardByCompanyIdException(str(e))

        except Exception as e:
            raise FetchBoardException(str(e))

    def validate_request(self, request: FetchBoardByCompanyIdRequest) -> bool:
        """
        Validate request
        """
        return True
