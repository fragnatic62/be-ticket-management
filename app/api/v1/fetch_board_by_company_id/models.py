from datetime import datetime
from typing import Optional, Sequence

from pydantic import BaseModel

from app.core.api.collections import DBCollectionEnum


class PostFetchBoardBaseModel(BaseModel):
    """
    Represents a base model for fetching a board.

    Attributes:
        _collection: collection name
    """
    _collection: str = DBCollectionEnum.BOARDS.value

    @property
    def collection(self) -> str:
        return self._collection


class FetchBoardByCompanyIdRequest(PostFetchBoardBaseModel):
    """
    Represents a request model for fetching a board by company id.

    Attributes:
        company_id: company id
    """
    company_id: str


class TaskResponse(BaseModel):
    """
    Represents a response model for creating a company tasks.

    Attributes:
        id: task id
        name: task name
        description: task description
        board_id: board id
        date_created: date created
        date_updated: date updated

    """
    id: str
    position: int
    name: str
    description: str
    board_id: str
    date_created: datetime
    date_updated: Optional[datetime] = None


class BoardResponse(BaseModel):
    """
    Represents a response model for creating a company task board.

    Attributes:
        name: board name
        position: board position
        description: board description
        date_created: date created
        date_updated: date updated
        tasks: board tasks
    """
    id: str
    position: int
    name: str
    description: str
    company_id: str
    date_created: datetime
    date_updated: Optional[datetime]
    tasks: Optional[Sequence[TaskResponse]] = []


class FetchBoardByCompanyIdResponse(BaseModel):
    """
    Represents a response model for fetching a board by company id.

    Attributes:
        boards: Sequence of boards
    """
    boards: Optional[Sequence[BoardResponse]] = []
