from app.core.common.base_exception import BaseCustomException


class FetchBoardByCompanyIdException(BaseCustomException):
    """
    Base exception for post_create_user
    """


class FetchBoardException(FetchBoardByCompanyIdException):
    """
    Raise when error occurs during creating user.
    """