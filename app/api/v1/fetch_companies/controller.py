from http import HTTPMethod
from typing import List

from pymongo.collection import Collection

from app.api.v1.fetch_companies.models import FetchCompaniesResponse, Company, FetchCompaniesRequest
from app.core.api.base_controller import BaseAPIController
from app.core.api.collections import DBCollectionEnum
from app.services.tm_db.provider import TMMongoDBServiceProvider


class APIController(
    BaseAPIController[FetchCompaniesRequest, FetchCompaniesResponse],
    TMMongoDBServiceProvider
):
    """
    **Synchronous API endpoint for fetching completeness summary**

    API request attributes:
        schedule_date (date): The date of the schedule.
        retailers (Array[str]): The list of retailers. e.g. ['amazon.com', 'walmart.com']

    """
    _full_dir: str = __file__
    api_tags: List[str] = ['Synchronous API']

    def get_path(self) -> str:
        return '/fetch_companies'

    def get_method(self) -> str:
        """
        return HTTP method intended
        """
        return HTTPMethod.POST

    def process_request(self, request: FetchCompaniesRequest) -> FetchCompaniesResponse:
        try:
            nosql_service: Collection = self.get_mongodb_service_from_collection(DBCollectionEnum.COMPANIES.value)
            total_count = nosql_service.count_documents({})
            q_response = nosql_service.find().skip((request.page - 1) * request.page_size).limit(request.page_size)

            companies: List[Company] = [
                Company(
                    id=str(company.get('_id')),
                    name=company.get('name'),
                    email=company.get('email'),
                    description=company.get('description'),
                    date_created=company.get('date_created'),
                    date_updated=company.get('date_updated')
                )
                for company in q_response
            ]

            return FetchCompaniesResponse(
                results=companies,
                page=request.page,
                page_size=request.page_size,
                total_count=total_count
            )
        except Exception as e:
            raise e

    def validate_request(self, request: FetchCompaniesRequest) -> bool:
        """
        Validate the email  field if it has record on Users and Auth collection.
        """
        return True
