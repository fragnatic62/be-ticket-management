from datetime import datetime
from typing import Optional, Sequence

from pydantic import BaseModel

from app.core.api.collections import DBCollectionEnum


class PostFetchTasksBaseModel(BaseModel):
    """
    Represents a base model for fetching tasks.

    Attributes:
        _collection: collection name
    """
    _collection: str = DBCollectionEnum.TASKS.value

    @property
    def collection(self) -> str:
        return self._collection


class PostFetchTasksRequest(PostFetchTasksBaseModel):
    """
    Represents a request model for fetching tasks by board id.

    Attributes:
        board_id: board id
    """
    board_id: str


class TaskResponse(BaseModel):
    """
    Represents a response model for creating a company tasks.

    Attributes:
        id: task id
        name: task name
        description: task description
        board_id: board id
        date_created: date created
        date_updated: date updated

    """
    id: str
    position: int
    name: str
    description: str
    board_id: str
    date_created: datetime
    date_updated: Optional[datetime] = None


class FetchTasksResponse(BaseModel):
    """
    Represents a response model for fetching tasks by board id.

    Attributes:
        tasks: tasks
    """
    tasks: Optional[Sequence[TaskResponse]] = []
