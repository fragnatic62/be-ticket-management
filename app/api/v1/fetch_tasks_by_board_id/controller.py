from http import HTTPMethod
from typing import List

from pymongo.collection import Collection

from app.api.v1.fetch_tasks_by_board_id.models import PostFetchTasksRequest, FetchTasksResponse, TaskResponse
from app.core.api.base_controller import BaseAPIController
from app.services.tm_db.provider import TMMongoDBServiceProvider


class APIController(
    BaseAPIController[PostFetchTasksRequest, FetchTasksResponse],
    TMMongoDBServiceProvider
):
    """
    **Synchronous API endpoint for fetching all tasks by board_id**

    Description:
        Fetches all tasks by board_id

    """
    _full_dir: str = __file__
    api_tags: List[str] = ['Synchronous API']

    def get_path(self) -> str:
        return '/post_fetch_tasks_by_board_id'

    def get_method(self) -> str:
        """
        return HTTP method intended
        """
        return HTTPMethod.POST

    def process_request(self, request: PostFetchTasksRequest) -> FetchTasksResponse:
        try:
            nosql_service: Collection = self.get_mongodb_service_from_collection(request.collection)
            tasks = nosql_service.find({'board_id': request.board_id})

            return FetchTasksResponse(
                tasks=[
                    TaskResponse(
                        id=str(task.get('_id')),
                        position=task.get('position'),
                        name=task.get('name'),
                        description=task.get('description'),
                        board_id=task.get('board_id'),
                        date_created=task.get('date_created'),
                        date_updated=task.get('date_updated')
                    )
                    for task in tasks
                ]
            )
        except Exception as e:
            raise e

    def validate_request(self, request: PostFetchTasksRequest) -> bool:
        """
        Validate the request
        """
        return True
