from app.core.common.base_exception import BaseCustomException


class PostBatchUpdateTasksException(BaseCustomException):
    """
    Base exception for post_create_user
    """


class BatchUpdateTasksException(PostBatchUpdateTasksException):
    """
    Raise when error occurs during creating user.
    """