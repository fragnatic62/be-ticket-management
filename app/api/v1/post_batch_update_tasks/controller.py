from http import HTTPMethod
from typing import List

from bson import ObjectId
from pymongo import UpdateOne
from pymongo.collection import Collection
from pymongo.errors import OperationFailure
from pymongo.results import BulkWriteResult

from app.core.api.base_controller import BaseAPIController
from app.services.tm_db.provider import TMMongoDBServiceProvider
from app.api.v1.post_batch_update_tasks.exceptions import PostBatchUpdateTasksException, BatchUpdateTasksException
from app.api.v1.post_batch_update_tasks.models import PostBatchUpdateTasksRequest, PostBatchUpdateTasksResponse


class APIController(
    BaseAPIController[PostBatchUpdateTasksRequest, PostBatchUpdateTasksResponse],
    TMMongoDBServiceProvider
):
    """
    **Synchronous API endpoint for fetching completeness summary**

    API request attributes:
        schedule_date (date): The date of the schedule.
        retailers (Array[str]): The list of retailers. e.g. ['amazon.com', 'walmart.com']

    """
    _full_dir: str = __file__
    api_tags: List[str] = ['Synchronous API']

    def get_path(self) -> str:
        return '/post_batch_update_tasks'

    def get_method(self) -> str:
        """
        return HTTP method intended
        """
        return HTTPMethod.POST

    def process_request(self, request: PostBatchUpdateTasksRequest) -> PostBatchUpdateTasksResponse:
        try:
            nosql_service: Collection = self.get_mongodb_service_from_collection(request.collection)
            q_response: BulkWriteResult = nosql_service.bulk_write([
                UpdateOne(
                    filter={'_id': ObjectId(task.id)},
                    update={
                        '$set': {
                            'position': task.position,
                            'name': task.name,
                            'description': task.description,
                            'board_id': task.board_id,
                            'date_updated': task.date_updated
                        }
                    }
                ) for task in request.tasks
            ])

            if q_response.modified_count == 0:
                raise PostBatchUpdateTasksException('No record updated.')

            return PostBatchUpdateTasksResponse(tasks=request.tasks)
        except OperationFailure:
            raise BatchUpdateTasksException('Error occurred during updating board.')

        except Exception as e:
            raise BatchUpdateTasksException(str(e))

    def validate_request(self, request: PostBatchUpdateTasksRequest) -> bool:
        """
        Validate the request
        """
        return True
