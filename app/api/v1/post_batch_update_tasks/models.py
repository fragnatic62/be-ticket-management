from datetime import datetime
from typing import Optional, Sequence

from pydantic import BaseModel, conlist

from app.core.api.collections import DBCollectionEnum


class BaseUpdateTaskBaseModel(BaseModel):
    """
    Represents a base model for creating a company.

    Attributes:
        _collection: collection name
    """
    _collection: str = DBCollectionEnum.TASKS.value

    @property
    def collection(self) -> str:
        return self._collection


class Task(BaseModel):
    """
    Represents a request model for creating a company task board.

    Attributes:
        id: task id
        name: task name
        description: task description
        position: task position
        board_id: board id
        date_updated: date updated
    """
    id: str
    position: int
    name: str
    description: str
    board_id: str
    date_updated: Optional[datetime] = datetime.utcnow()


class PostBatchUpdateTasksRequest(BaseUpdateTaskBaseModel):
    """
    Represents a request model for creating a board tasks.

    Attributes:
        tasks: list of tasks
    """
    tasks: conlist(Task, min_length=1)


class PostBatchUpdateTasksResponse(BaseModel):
    """
    Represents a response model for creating a company task board.

    Attributes:
        tasks: list of tasks
    """
    tasks: Sequence[Task]
