from datetime import datetime
from http import HTTPMethod
from typing import List, Sequence

from bson import ObjectId
from pymongo.collection import Collection
from pymongo.errors import OperationFailure

from app.api.v1.post_update_board.exceptions import PostUpdateBoardException, UpdateBoardException
from app.api.v1.post_update_board.models import PostUpdateBoardRequest, PostUpdateBoardResponse, TaskResponse
from app.core.api.base_controller import BaseAPIController
from app.core.api.collections import DBCollectionEnum
from app.services.tm_db.provider import TMMongoDBServiceProvider


class APIController(
    BaseAPIController[PostUpdateBoardRequest, PostUpdateBoardResponse],
    TMMongoDBServiceProvider
):
    """
    **Synchronous API endpoint for fetching completeness summary**

    API request attributes:
        schedule_date (date): The date of the schedule.
        retailers (Array[str]): The list of retailers. e.g. ['amazon.com', 'walmart.com']

    """
    _full_dir: str = __file__
    api_tags: List[str] = ['Synchronous API']

    def get_path(self) -> str:
        return '/post_update_board'

    def get_method(self) -> str:
        """
        return HTTP method intended
        """
        return HTTPMethod.POST

    def fetch_tasks(self, board_id: str) -> Sequence[TaskResponse]:
        """
        Fetch all tasks by board id
        """
        nosql_service: Collection = self.get_mongodb_service_from_collection(DBCollectionEnum.TASKS.value)
        tasks = nosql_service.find({'board_id': board_id}).sort('position', 1)
        return [
            TaskResponse(
                id=str(task['_id']),
                position=task['position'],
                name=task['name'],
                description=task['description'],
                board_id=task['board_id'],
                date_created=task['date_created'],
                date_updated=task['date_updated']
            ) for task in tasks
        ]

    def process_request(self, request: PostUpdateBoardRequest) -> PostUpdateBoardResponse:
        try:
            nosql_service: Collection = self.get_mongodb_service_from_collection(request.collection)
            q_response = nosql_service.update_one(
                {'_id': ObjectId(request.id)},
                {
                    '$set': {
                        'position': request.position,
                        'name': request.name,
                        'description': request.description,
                        'date_updated': datetime.utcnow()
                    }
                }
            )

            if q_response.modified_count == 0:
                raise PostUpdateBoardException('No record updated.')

            return PostUpdateBoardResponse(
                id=request.id,
                position=request.position,
                name=request.name,
                description=request.description,
                company_id=request.company_id,
                date_created=request.date_created,
                date_updated=request.date_updated,
                tasks=self.fetch_tasks(request.id)
            )
        except OperationFailure:
            raise PostUpdateBoardException('Error occurred during updating board.')

        except Exception as e:
            raise UpdateBoardException(str(e))

    def validate_request(self, request: PostUpdateBoardRequest) -> bool:
        """
        Validate the request
        """
        return True
