from http import HTTPMethod
from typing import List

from bson import ObjectId
from pymongo import UpdateOne
from pymongo.collection import Collection
from pymongo.errors import OperationFailure
from pymongo.results import BulkWriteResult

from app.api.v1.post_update_board.models import PostUpdateBoardRequest
from app.core.api.base_controller import BaseAPIController
from app.services.tm_db.provider import TMMongoDBServiceProvider
from app.api.v1.post_batch_update_boards.exceptions import PostBatchUpdateBoardException, BatchUpdateBoardException
from app.api.v1.post_batch_update_boards.models import PostBatchUpdateBoardRequest, PostBatchUpdateBoardResponse


class APIController(
    BaseAPIController[PostBatchUpdateBoardRequest, PostBatchUpdateBoardResponse],
    TMMongoDBServiceProvider
):
    """
    **Synchronous API endpoint for fetching completeness summary**

    API request attributes:
        schedule_date (date): The date of the schedule.
        retailers (Array[str]): The list of retailers. e.g. ['amazon.com', 'walmart.com']

    """
    _full_dir: str = __file__
    api_tags: List[str] = ['Synchronous API']

    def get_path(self) -> str:
        return '/post_batch_update_boards'

    def get_method(self) -> str:
        """
        return HTTP method intended
        """
        return HTTPMethod.POST

    def process_request(self, request: PostBatchUpdateBoardRequest) -> PostBatchUpdateBoardResponse:
        try:
            nosql_service: Collection = self.get_mongodb_service_from_collection(request.collection)
            q_response: BulkWriteResult = nosql_service.bulk_write([
                UpdateOne(
                    filter={'_id': ObjectId(board.id)},
                    update={
                        '$set': {
                            'position': board.position,
                            'name': board.name,
                            'description': board.description,
                            'date_updated': board.date_updated,
                            'company_id': board.company_id
                        }
                    }
                ) for board in request.boards
            ])

            if q_response.modified_count == 0:
                raise PostBatchUpdateBoardException('No record updated.')

            return PostBatchUpdateBoardResponse(boards=request.boards)
        except OperationFailure:
            raise BatchUpdateBoardException('Error occurred during updating board.')

        except Exception as e:
            raise BatchUpdateBoardException(str(e))

    def validate_request(self, request: PostUpdateBoardRequest) -> bool:
        """
        Validate the request
        """
        return True
