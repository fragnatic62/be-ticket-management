from app.core.common.base_exception import BaseCustomException


class PostBatchUpdateBoardException(BaseCustomException):
    """
    Base exception for post_create_user
    """


class BatchUpdateBoardException(PostBatchUpdateBoardException):
    """
    Raise when error occurs during creating user.
    """