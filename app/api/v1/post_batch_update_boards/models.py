from datetime import datetime
from typing import Optional

from pydantic import BaseModel, conlist

from app.core.api.collections import DBCollectionEnum


class PostUpdateBoardBaseModel(BaseModel):
    """
    Represents a base model for creating a company.

    Attributes:
        _collection: collection name
    """
    _collection: str = DBCollectionEnum.BOARDS.value

    @property
    def collection(self) -> str:
        return self._collection


class Board(BaseModel):
    """
    Represents a request model for creating a company task board.

    Attributes:
        position: board position
        name: board name
        description: board description
        date_updated: date updated
    """
    id: str
    company_id: str
    position: int
    name: str
    description: str
    date_updated: Optional[datetime] = datetime.utcnow()


class PostBatchUpdateBoardRequest(PostUpdateBoardBaseModel):
    """
    Represents a request model for creating a company task board.

    Attributes:
        boards: board list
    """
    boards: conlist(Board, min_length=1)


class PostBatchUpdateBoardResponse(BaseModel):
    """
    Represents a response model for updating a company board.
    """
    boards: conlist(Board, min_length=1)
