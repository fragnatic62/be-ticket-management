# Use an official Python runtime as a parent image
FROM python:3.11.4-slim-bullseye

# Turns off buffering for easier container logging
ENV PYTHONDONTWRITEBYTECODE=1

ENV PYTHONUNBUFFERED=1

# Install CURL
RUN apt-get update
RUN apt-get install -y curl gnupg2 g++

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container to WORKDIR
COPY . /app

# RUN pip install -r requirements.txt
RUN pip install -r /app/requirements.txt

# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
CMD ["uvicorn app.main:app"]